-- MySQL dump 10.13  Distrib 8.0.23, for osx10.16 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `test`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `test` /*!40100 DEFAULT CHARACTER SET utf8 */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `test`;

--
-- Table structure for table `attributes`
--

DROP TABLE IF EXISTS `attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `attributes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attributes`
--

LOCK TABLES `attributes` WRITE;
/*!40000 ALTER TABLE `attributes` DISABLE KEYS */;
INSERT INTO `attributes` VALUES (1,'fullname'),(2,'avatar'),(3,'email'),(4,'password'),(5,'address'),(6,'zipcode'),(7,'phone');
/*!40000 ALTER TABLE `attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_brands`
--

DROP TABLE IF EXISTS `device_brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_brands` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_brands`
--

LOCK TABLES `device_brands` WRITE;
/*!40000 ALTER TABLE `device_brands` DISABLE KEYS */;
INSERT INTO `device_brands` VALUES (1,'Samsung','2019-04-18 22:17:00'),(2,'Motorola','2019-04-18 22:17:00'),(3,'Apple','2019-04-18 22:17:00');
/*!40000 ALTER TABLE `device_brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `device_screensizes`
--

DROP TABLE IF EXISTS `device_screensizes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `device_screensizes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `device_screensizes`
--

LOCK TABLES `device_screensizes` WRITE;
/*!40000 ALTER TABLE `device_screensizes` DISABLE KEYS */;
INSERT INTO `device_screensizes` VALUES (1,'1440×2560','2019-04-18 22:17:00'),(2,'750×1334','2019-04-18 22:17:00'),(3,'720×1280','2019-04-18 22:17:00');
/*!40000 ALTER TABLE `device_screensizes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `devices` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_device_brand` int unsigned NOT NULL,
  `id_device_screensize` int unsigned NOT NULL,
  `description` varchar(128) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES (1,1,2,'Samsung S7','2019-04-18 22:17:01'),(2,1,1,'Samsung S8','2019-04-18 22:17:01'),(3,1,1,'Samsung S9','2019-04-18 22:17:01'),(4,3,1,'iPhone 8','2019-04-18 22:17:01');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurements`
--

DROP TABLE IF EXISTS `measurements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `measurements` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_metric` int unsigned NOT NULL,
  `id_unit` int unsigned NOT NULL,
  `amount` decimal(16,4) unsigned NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurements`
--

LOCK TABLES `measurements` WRITE;
/*!40000 ALTER TABLE `measurements` DISABLE KEYS */;
INSERT INTO `measurements` VALUES (1,1,1,37.1200,'2019-04-18 22:17:01'),(2,1,1,36.2400,'2019-04-18 22:17:01'),(3,1,1,38.7100,'2019-04-18 22:17:01'),(4,1,1,35.2900,'2019-04-18 22:17:01'),(5,1,1,38.8100,'2019-04-18 22:17:01'),(6,1,1,37.4100,'2019-04-18 22:17:01'),(7,1,1,36.5600,'2019-04-18 22:17:01'),(8,1,1,37.0500,'2019-04-18 22:17:01');
/*!40000 ALTER TABLE `measurements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metrics`
--

DROP TABLE IF EXISTS `metrics`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metrics` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metrics`
--

LOCK TABLES `metrics` WRITE;
/*!40000 ALTER TABLE `metrics` DISABLE KEYS */;
INSERT INTO `metrics` VALUES (1,'humidity'),(2,'temperature'),(3,'precipitations'),(4,'voltage');
/*!40000 ALTER TABLE `metrics` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_channels`
--

DROP TABLE IF EXISTS `notification_channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_channels` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_channels`
--

LOCK TABLES `notification_channels` WRITE;
/*!40000 ALTER TABLE `notification_channels` DISABLE KEYS */;
INSERT INTO `notification_channels` VALUES (1,'sms'),(2,'email'),(3,'push_notification'),(4,'slack');
/*!40000 ALTER TABLE `notification_channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_statuses`
--

DROP TABLE IF EXISTS `notification_statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_statuses` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_statuses`
--

LOCK TABLES `notification_statuses` WRITE;
/*!40000 ALTER TABLE `notification_statuses` DISABLE KEYS */;
INSERT INTO `notification_statuses` VALUES (1,'pending'),(2,'sent'),(3,'cancelled'),(4,'failure');
/*!40000 ALTER TABLE `notification_statuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_template_types`
--

DROP TABLE IF EXISTS `notification_template_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_template_types` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_template_types`
--

LOCK TABLES `notification_template_types` WRITE;
/*!40000 ALTER TABLE `notification_template_types` DISABLE KEYS */;
INSERT INTO `notification_template_types` VALUES (1,'raw'),(2,'filename');
/*!40000 ALTER TABLE `notification_template_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification_templates`
--

DROP TABLE IF EXISTS `notification_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification_templates` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_notification_template_type` int unsigned NOT NULL COMMENT 'Puede ser la url archivo, el texto, etc.',
  `description` varchar(128) NOT NULL,
  `body` varchar(512) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_notification_template_type` (`id_notification_template_type`),
  CONSTRAINT `notification_templates_ibfk_1` FOREIGN KEY (`id_notification_template_type`) REFERENCES `notification_template_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification_templates`
--

LOCK TABLES `notification_templates` WRITE;
/*!40000 ALTER TABLE `notification_templates` DISABLE KEYS */;
INSERT INTO `notification_templates` VALUES (1,1,'code.validation','<%= data.prefix %> Tu codigo de Witta es: <%= data.code %> <%= data.hash %>'),(2,1,'debug.code.validation','User [<%= data.user.id %>] - Code [<%= data.code %>]'),(3,2,'user.welcome','users/welcome'),(4,2,'users.logins.email.validation','users/validation'),(5,2,'gitlab.task','gitlab/task'),(30,1,'links.payments','Solicitud de dinero. <%= data.amount %>.\nPagalos con este link de pago de Witta: <%= data.uri %>');
/*!40000 ALTER TABLE `notification_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `id_notification_template` int unsigned NOT NULL,
  `id_notification_channel` int unsigned NOT NULL,
  `id_notification_status` int unsigned NOT NULL,
  `metadata` json NOT NULL COMMENT 'El objeto json contendrá toda la información de la notificación.',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_notification_template` (`id_notification_template`),
  KEY `id_notification_channel` (`id_notification_channel`),
  KEY `id_notification_status` (`id_notification_status`),
  CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`id_notification_template`) REFERENCES `notification_templates` (`id`),
  CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`id_notification_channel`) REFERENCES `notification_channels` (`id`),
  CONSTRAINT `notifications_ibfk_3` FOREIGN KEY (`id_notification_status`) REFERENCES `notification_statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` VALUES (1,5,4,2,'{\"to\": \"test-cli-notifications\", \"response\": \"ok\"}','2020-04-12 20:11:51','2020-04-12 20:11:51'),(2,5,4,2,'{\"to\": \"test-cli-notifications\", \"project\": \"Gonza Cara de Gato\", \"response\": \"ok\"}','2020-04-12 20:12:54','2020-04-12 20:12:55'),(3,5,4,2,'{\"to\": \"test-cli-notifications\", \"project\": \"Gonza Cara de Gato\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:45:21','2020-04-12 20:45:21'),(4,5,4,2,'{\"to\": \"test-cli-notifications\", \"task\": \"Lint & Tests\", \"result\": \"success\", \"project\": \"FintechEventsMicroservice\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:46:32','2020-04-12 20:46:32'),(5,5,4,2,'{\"to\": \"test-cli-notifications\", \"task\": \"Lint & Tests\", \"result\": \"success\", \"project\": \"FintechEventsMicroservice\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:48:57','2020-04-12 20:48:58'),(6,5,4,2,'{\"to\": \"test-cli-notifications\", \"task\": \"Lint & Tests\", \"result\": \"success\", \"project\": \"FintechEventsMicroservice\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:50:30','2020-04-12 20:50:30'),(7,5,4,2,'{\"to\": \"test-cli-notifications\", \"task\": \"Lint & Tests\", \"result\": \"success\", \"project\": \"FintechEventsMicroservice\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:52:16','2020-04-12 20:52:17'),(8,5,4,2,'{\"to\": \"test-cli-notifications\", \"task\": \"Lint & Tests\", \"result\": \"success\", \"project\": \"FintechEventsMicroservice\", \"response\": \"ok\", \"environment\": \"testing\"}','2020-04-12 20:54:24','2020-04-12 20:54:25');
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `organizations`
--

DROP TABLE IF EXISTS `organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `organizations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `organizations`
--

LOCK TABLES `organizations` WRITE;
/*!40000 ALTER TABLE `organizations` DISABLE KEYS */;
INSERT INTO `organizations` VALUES (1,1,'2019-04-18 22:17:01',NULL),(2,1,'2019-04-18 22:17:01',NULL),(3,1,'2019-04-18 22:17:01',NULL);
/*!40000 ALTER TABLE `organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'owner'),(2,'admin'),(3,'editor'),(4,'readonly');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` tinyint unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'Weather Station'),(2,'Smart Bulb'),(3,'Smart Lock'),(4,'Humidity Spear'),(5,'Accelerometer');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `units` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(128) NOT NULL,
  `unit` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `units`
--

LOCK TABLES `units` WRITE;
/*!40000 ALTER TABLE `units` DISABLE KEYS */;
INSERT INTO `units` VALUES (1,'percentage','%'),(2,'celsius','Cº'),(3,'millivolt','mV');
/*!40000 ALTER TABLE `units` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_attributes`
--

DROP TABLE IF EXISTS `user_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_attributes` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int unsigned NOT NULL,
  `id_attribute` int unsigned NOT NULL,
  `description` varchar(256) NOT NULL,
  `active` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `id_user` (`id_user`),
  KEY `id_attribute` (`id_attribute`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_attributes`
--

LOCK TABLES `user_attributes` WRITE;
/*!40000 ALTER TABLE `user_attributes` DISABLE KEYS */;
INSERT INTO `user_attributes` VALUES (1,1,1,'John Doe',1,'2019-04-18 22:17:01',NULL),(2,1,3,'john.doe@gmail.com',1,'2019-04-18 22:17:01',NULL),(3,1,4,'9959a1c23dcc98a7b3ca78dfd777d7ad',1,'2019-04-18 22:17:01',NULL),(4,2,1,'Mario Speedwagon',1,'2019-04-18 22:17:01',NULL),(5,2,3,'mario.speedwagon@gmail.com',1,'2019-04-18 22:17:01',NULL),(6,2,4,'fbbaad48686fa8f3a6e1d0c92b301da9',1,'2019-04-18 22:17:01',NULL),(7,3,1,'Anna Sthesia',1,'2019-04-18 22:17:01',NULL),(8,3,3,'anna.sthesia@gmail.com',1,'2019-04-18 22:17:01',NULL),(9,3,4,'433572aec18e8f74f4ddd2ba72b69144',1,'2019-04-18 22:17:01',NULL),(10,3,1,'Paul Molive',1,'2019-04-18 22:17:01',NULL),(11,3,3,'paul.molive@gmail.com',1,'2019-04-18 22:17:01',NULL),(12,3,4,'c4e3013d6faa2ebaf904a76140e7050e',1,'2019-04-18 22:17:01',NULL),(13,3,1,'Anna Mull',1,'2019-04-18 22:17:01',NULL),(14,3,3,'anna.mull@gmail.com',1,'2019-04-18 22:17:01',NULL),(15,3,4,'039ac5d5fb2ff5039e57a158a6080bed',1,'2019-04-18 22:17:01',NULL),(16,3,1,'Gail Forcewind',1,'2019-04-18 22:17:01',NULL),(17,3,3,'gail.forcewind@gmail.com',1,'2019-04-18 22:17:01',NULL),(18,3,4,'513a0fc87d6aa8625cc2b6b09a85563e',1,'2019-04-18 22:17:01',NULL),(19,3,1,'Paige Turner',1,'2019-04-18 22:17:01',NULL),(20,3,3,'paige.turner@gmail.com',1,'2019-04-18 22:17:01',NULL),(21,3,4,'b5bef18676056fea7f2e4b727bf8b91f',1,'2019-04-18 22:17:01',NULL),(22,3,1,'Bob Frapples',1,'2019-04-18 22:17:01',NULL),(23,3,3,'bob.frapples@gmail.com',1,'2019-04-18 22:17:01',NULL),(24,3,4,'4d1a3a7de62d988b6a0f00748c54c375',1,'2019-04-18 22:17:01',NULL);
/*!40000 ALTER TABLE `user_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_permissions`
--

DROP TABLE IF EXISTS `user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_permissions` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `id_user` int unsigned NOT NULL,
  `id_permission` int unsigned NOT NULL,
  `active` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_user.id_permission` (`id_user`,`id_permission`),
  KEY `id_user` (`id_user`),
  KEY `id_permission` (`id_permission`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_permissions`
--

LOCK TABLES `user_permissions` WRITE;
/*!40000 ALTER TABLE `user_permissions` DISABLE KEYS */;
INSERT INTO `user_permissions` VALUES (1,1,1,1,'2019-04-18 22:17:01',NULL),(2,2,2,1,'2019-04-18 22:17:01',NULL),(3,3,3,1,'2019-04-18 22:17:01',NULL),(4,4,1,1,'2019-04-18 22:17:01',NULL),(5,5,3,1,'2019-04-18 22:17:01',NULL),(6,6,1,1,'2019-04-18 22:17:01',NULL),(7,7,3,1,'2019-04-18 22:17:01',NULL);
/*!40000 ALTER TABLE `user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `active` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'2019-04-18 22:17:01',NULL),(2,1,'2019-04-18 22:17:01',NULL),(3,1,'2019-04-18 22:17:01',NULL),(4,1,'2020-04-02 23:57:24',NULL),(6,1,'2020-04-02 23:57:42',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-06 12:15:14
