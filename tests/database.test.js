'use strict';

const { Database } = require('../core');

const defaultOptions = {
    database: 'test',
    username: 'root',
    password: 'WwFFTRDJ7s2RgPWx',
    host: 'localhost'
};

let db;

describe('@comodinx/sequelize', () => {
    describe('Instance', () => {
        beforeEach(() => {
            db = new Database(defaultOptions);
        });

        afterEach(done => {
            db.end().then(done);
        });

        it('should return all tables on database', done => {
            db
                .query('SHOW TABLES')
                .then(result => {
                    expect(result).to.be.a('array').to.not.be.empty; // eslint-disable-line no-unused-expressions

                    done();
                })
                .catch(done);
        });

        it('should return only one table of database', done => {
            db
                .queryOne('SHOW TABLES')
                .then(result => {
                    expect(result).to.be.a('object').to.have.property(`Tables_in_${defaultOptions.database}`);

                    done();
                })
                .catch(done);
        });

        it('should return only one table of database on transaction mode', done => {
            db
                .transaction(transaction => {
                    return db.queryOne('SHOW TABLES', { transaction }).then(result => {
                        expect(result).to.be.a('object').to.have.property(`Tables_in_${defaultOptions.database}`);

                        done();
                    });
                })
                .catch(done);
        });

        it('should check if database connection is alive', done => {
            db
                .isAlive()
                .then(result => {
                    expect(result).to.be.a('boolean').equal(true);

                    done();
                })
                .catch(done);
        });

        it('should check if database connection resolve ping', done => {
            db
                .ping()
                .then(result => {
                    expect(result).to.be.a('boolean').equal(true);

                    done();
                })
                .catch(done);
        });
    });
});
